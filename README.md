# Setup

## Requirement
- NodeJS: version 4.x or higher
- MongoDB: version 3.x
- Forever: lastest (NodeJS module install globally by command: npm install -g forever)

## Config
Change the .env.development file for:
- PORT: port that server will listen on
- APP_URL: base url of server request
- URI_MONGO: connection string for mongodb

## Install modules
In the server folder, run "npm install"

## Run server
forever start [path_to_server_folder]/server.js
