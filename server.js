//params
global.Promise = require('bluebird');
var dotenv = require('dotenv');
dotenv.load({ path: `.env.${process.env.NODE_ENV || 'development'}` });
var helpers = require('./helpers');
var fs = require('fs');

//setup express
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').Server(app);

//setup moongoose
require('./mongoose')();
var MessageModel = require('./models/message');

//setup socket
var io = require('socket.io')(server);
server.listen(process.env.PORT || 3000);

app.use(bodyParser.json());

//server statistic file file
app.use('/assets', express.static(__dirname + '/public'));
app.get('/', (req, res) => {
    res.header('Content-Type', 'text/html');
    return res.send(fs.readFileSync(helpers.getPublicPath('index.html')));
});

//routes
app.get('/messages', (req, res) => {
    var dateFrom = new Date(req.query.date);
    if (!dateFrom.getTime()) {
        dateFrom = new Date();
    }
    return MessageModel.find({
            createdAt: { $lte: dateFrom },
            _id: { $nin: helpers.getOjectIds(req.query.ids) }
        }).sort({ createdAt: -1 }).skip(0).limit(20)
        .then(results => {
            return res.json({
                code: 'success',
                data: results || []
            });
        })
        .catch(err => {
            console.error(err);
            return res.json({
                code: 'success',
                data: []
            });
        });
});
app.post('/chat', (req, res) => {
    return MessageModel.create({
            name: req.body.name,
            content: req.body.content
        })
        .then(message => {
            if (!message) {
                return Promise.reject();
            }
            io.emit('new messaged', message);
            return res.json({ code: 'success', data: message });
        })
        .catch(err => {
            console.error('/upload');
            console.error(err);
            return res.json({ code: 'error', data: err.message || 'Error' })
        });
});

//socket handle
io.on('connection', socket => {
    socket.join('general');
    socket.on('joining', data => {
        console.log(data);
        io.emit('new messaged', {
            name: 'Chat Bot',
            content: `"${data.name}" joining`
        });
    });
    socket.on('leaving', data => {
        io.emit('new messaged', {
            name: 'Chat Bot',
            content: `"${data.name}" leaving`
        });
    });
});
