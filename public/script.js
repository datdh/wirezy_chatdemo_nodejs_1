const socket = io();
function scrollToLastMessage(toTop) {
    setTimeout(() => {
        const elem = document.querySelector('.msg-wrap');
        elem.scrollTop = toTop ? 10 : elem.scrollHeight;
    }, 300);
}
let app = new Vue({
    el: '#chat',
    data: {
        messages: [],
        inputMessage: '',
        fullName: '',
        loadOldMessageOpts: {
            oldestTime: new Date(),
            oldestIds: [],
            canLoadOldMessage: true,
        }
    },
    watch: {
        fullName(){
            localStorage.setItem('fullName', this.fullName);
        }
    },
    methods: {
        init(){
            this.fullName = localStorage.getItem('fullName');
            this.loadOldMessages(false);
            if (!this.fullName) {
                const nameDefault = `Guest-${Date.now()}`;
                bootbox.prompt({
                    title: 'Nhập tên bạn muốn: ',
                    value: nameDefault,
                    callback: (value) => {
                        value = _.trim(value);
                        if (value.length < 3) {
                            this.fullName = nameDefault;
                            return;
                        }
                        this.fullName = value;
                        socket.emit('joining', {
                            name: this.fullName
                        });
                    }
                });
            }
        },
        loadOldMessages(toTop, replace){
            const options = this.loadOldMessageOpts;
            if (!options.canLoadOldMessage) return;
            this.$http.get(
                `/messages?date=${options.oldestTime}&ids=${options.oldestIds.join(',')}`
            )
                .then(response => {
                    let body = response.body;
                    if (!body || body.code !== 'success' || !Array.isArray(body.data)) return;
                    if (!body.data.length) {
                        this.loadOldMessageOpts.canLoadOldMessage = false;
                        return;
                    }
                    body.data.reverse();
                    this.loadOldMessageOpts.oldestTime = body.data[0].createdAt;
                    body.data.some(message => {
                        if (message.createdAt < options.oldestTime) {
                            return true;
                        }
                        this.loadOldMessageOpts.oldestIds.push(message._id);
                    });
                    if (replace) {
                        this.messages = body.data;
                    } else {
                        this.messages.unshift.apply(this.messages, body.data);
                    }
                    scrollToLastMessage(toTop);
                });
        },
        sendMessage(){
            const inputMessage = _.trim(this.inputMessage);
            if (inputMessage.length < 3) {
                this.inputMessage = inputMessage;
                return;
            }
            this.$http.post('/chat', {name: this.fullName, content: inputMessage})
                .then(response => {
                    this.inputMessage = '';
                    if (response.body && response.body.code === 'error') {
                        alert(response.body.data)
                    }
                });
        },
        appendMessage(message){
            this.messages.push(message);
            scrollToLastMessage();
        },
    }
});
app.init();
socket.on('new messaged', message => {
    app.appendMessage(message);
});
const msgWrap = document.querySelector('.msg-wrap');
msgWrap.onscroll = () => {
    if (msgWrap.scrollTop === 0) {
        app.loadOldMessages(true);
    }
};
