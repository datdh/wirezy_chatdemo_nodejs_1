var mongoose;
module.exports = () => {
    if (mongoose) {
        return mongoose;
    }
    mongoose = require('mongoose');
    mongoose.Promise = require('bluebird');

    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

    mongoose.connection.once('open', () => {
        console.info('Connected to DB!');
    });
    mongoose.connect(process.env.URI_MONGO);

    return mongoose;
};
