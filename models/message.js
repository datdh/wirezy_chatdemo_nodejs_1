var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var MessageSchema = new Schema({
    name: String,
    content: String
}, {
    timestamps: true,
    versionKey: false
});
module.exports = mongoose.model('Message', MessageSchema);
