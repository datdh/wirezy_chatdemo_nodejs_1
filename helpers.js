var path = require('path');
var mongoose = require('mongoose');

module.exports = {
    getPublicPath(fileName){
        return path.join(process.cwd(), 'public', fileName);
    },
    getOjectIds(str){
        if (!str || str.length < 24)return [];
        var results = [];
        str.split(',').forEach(id => {
            try {
                results.push(mongoose.Types.ObjectId(id));
            } catch (e) {
            }
        });
        return results;
    },
};
